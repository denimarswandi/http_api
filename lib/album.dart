import 'dart:convert';

class Album {
  final int userId;
  final int id;
  final String title;

  Album({this.userId, this.id, this.title});

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(userId: json["userId"], id: json["id"], title: json['title']);
  }
}

List<Album> albumFromJsonAll(String jsonData) {
  final data = json.decode(jsonData);
  return List<Album>.from(data.map((item) => Album.fromJson(item)));
}

class AlbumPost {
  final int id;
  final String title;

  AlbumPost({this.id, this.title});

  factory AlbumPost.fromJson(Map<String, dynamic> json) {
    return AlbumPost(id: json['id'], title: json['title']);
  }
}
