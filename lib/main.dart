import 'package:flutter/material.dart';
import 'package:http_api/apiservice.dart';
import 'package:http_api/album.dart';
import 'package:http_api/two.dart';
import 'package:http_api/three.dart';

void main() {
  runApp(MaterialApp(
    title: "Hello",
    home: HTTPReq(),
  ));
}

class HTTPReq extends StatefulWidget {
  @override
  _HTTPReqState createState() => _HTTPReqState();
}

class _HTTPReqState extends State<HTTPReq> {
  ApiService apiService;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test"),
        backgroundColor: Colors.orange,
      ),
      body: Container(
        child: Center(
          child: FutureBuilder<Album>(
            future: apiService.fetchAlbum(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(children: <Widget>[
                  Text(snapshot.data.title),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Two()));
                    },
                    child: Text('TWO PAGE'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Three()));
                    },
                    child: Text('THREE PAGE'),
                  )
                ]);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
