import 'package:flutter/material.dart';
import 'package:http_api/apiservice.dart';
import 'package:http_api/album.dart';

class Two extends StatefulWidget {
  @override
  _TwoState createState() => _TwoState();
}

class _TwoState extends State<Two> {
  ApiService apiService;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
  }

  @override
  Widget build(BuildContext context) {
    //apiService.getAll().then((value) => print(value));
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Text("Page Two"),
        ),
        body: Container(
          child: FutureBuilder(
            future: apiService.getAll(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Album>> snapshot) {
              if (snapshot.hasData) {
                List<Album> albums = snapshot.data;
                return _buildList(albums);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ));
  }

  Widget _buildList(List<Album> albums) {
    return Container(
      child: ListView.builder(
        itemCount: albums.length,
        itemBuilder: (context, index) {
          Album album = albums[index];
          return Card(
            child: Column(
              children: <Widget>[
                Text(album.userId.toString()),
                Text(album.title),
                Text(album.id.toString())
              ],
            ),
          );
        },
      ),
    );
  }
}
