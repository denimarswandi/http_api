import 'package:flutter/material.dart';
import 'package:http_api/album.dart';
import 'package:http_api/apiservice.dart';

class Three extends StatefulWidget {
  @override
  _ThreeState createState() => _ThreeState();
}

class _ThreeState extends State<Three> {
  final TextEditingController _controller = TextEditingController();
  Future<AlbumPost> _futureAlbum;
  ApiService apiService;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiService = ApiService();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Three Page"),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8.0),
        child: (_futureAlbum == null)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    controller: _controller,
                    decoration: InputDecoration(hintText: 'Title'),
                  ),
                  RaisedButton(
                    child: Text('Create Data'),
                    onPressed: (() {
                      setState(() {
                        _futureAlbum = apiService.createAlbum(_controller.text);
                      });
                    }),
                  )
                ],
              )
            : FutureBuilder<AlbumPost>(
                future: _futureAlbum,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return (Text(snapshot.data.title));
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }

                  return CircularProgressIndicator();
                },
              ),
      ),
    );
  }
}
