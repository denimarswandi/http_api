import 'package:http_api/album.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiService {
  static const url = "https://jsonplaceholder.typicode.com";

  Future<Album> fetchAlbum() async {
    final response = await http.get('$url/albums/1');
    if (response.statusCode == 200) {
      return Album.fromJson(json.decode(response.body));
    } else {
      throw Exception('uuups');
    }
  }

  Future<List<Album>> getAll() async {
    final response = await http.get('$url/albums');
    if (response.statusCode == 200) {
      return albumFromJsonAll(response.body);
    } else {
      throw Exception("OOOppppssss");
    }
  }

  Future<AlbumPost> createAlbum(String title) async {
    final response = await http.post('$url/albums',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, String>{
          'title': title,
        }));
    if (response.statusCode == 201) {
      return AlbumPost.fromJson(json.decode(response.body));
    } else {
      throw Exception('failed');
    }
  }
}
